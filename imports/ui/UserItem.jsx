

import React, {Component, PropTypes} from 'react';
import ListItem from 'grommet/components/ListItem';
import {Messages} from '../api/messages.js';
import {createContainer} from 'meteor/react-meteor-data';
import Status from 'grommet/components/icons/Status';

class UserItem extends Component {

    constructor() {
        super();
        this.state = {
            time: new Date().getTime()
        };
    }

    componentDidMount() { // actualiza el texto de estado en la base de datos
        Meteor.setInterval(function() {
            ourThis.setState({time: new Date().getTime()})
        }, 1000);
    }

    render() {
        const lastMessageStyle = {
            fontSize: 11
        };
        const unreadStyle = {
            fontSize: 11,
            fontWeight: 'bold'
        }
        

        let lastMessageItem = <div/> // ultimo mensaje  
        if (this.props.lastMessage) {
            if (this.props.lastMessage.to == this.props.currentUser._id) {
                if (this.props.lastMessage.read) {
                    lastMessageItem = <div style={lastMessageStyle}>{this.props.lastMessage.text}</div>
                } else {
                    lastMessageItem = <div style={unreadStyle}>{this.props.lastMessage.text}</div>
                }
            } else {
                lastMessageItem = <div style={lastMessageStyle}>{this.props.lastMessage.text}</div>
            }
        }

        return <ListItem onClick={() => this.props.onSelect(this.props.selectedUser)}>
            <div>
                <div>
                    {this.props.selectedUser.username}
                    {this.props.selectedUser.lastTypingTime && this.props.selectedUser.lastTypingTime[this.props.currentUser._id]
                        ? (this.state.time - this.props.selectedUser.lastTypingTime[this.props.currentUser._id].getTime()) < 1000
                            ? " - typing..."
                            : ""
                        : ""}
                </div>
                {lastMessageItem}
            </div>
        </ListItem>

    }
}


UserItem.propTypes = {
  lastMessage: React.PropTypes.object,
  currentUser: React.PropTypes.object.isRequired,
  selectedUser: React.PropTypes.object.isRequired
};


export default createContainer(({currentUser, selectedUser}) => {

    Meteor.subscribe('messages');

    return {
        lastMessage: Messages.findOne({
            $or: [
                {
                    from: currentUser._id,
                    to: selectedUser._id
                }, {
                    from: selectedUser._id,
                    to: currentUser._id
                }
            ]
        }, {
            sort: {
                time: -1,
                limit: 1
            }
        })
    };
}, UserItem);
