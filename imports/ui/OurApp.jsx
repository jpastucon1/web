/*

Este es el componente principal de la aplicación. 
Mostrará un cuadro de inicio de sesión o la aplicación de chat.

*/

import React, { Component, PropTypes } from 'react';

import App from 'grommet/components/App';
import 'grommet/grommet.min.css';
import { Accounts } from 'meteor/std:accounts-basic';
import { createContainer } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import ChatApp from './ChatApp';
import Users from '../api/users.js';

T9n.setLanguage('es');

Accounts.ui.config({
  passwordSignupFields: 'USERNAME_AND_OPTIONAL_EMAIL'
});

class OurApp extends Component {

  render() { // si está logeado, mostrara la aplicación de chat, de lo contrario, mostrara el formulario de inicio de sesión
    return (
        <App>
          {this.props.currentUser ?
          <ChatApp currentUser={this.props.currentUser} allUsers={this.props.allUsers} /> : <Accounts.ui.LoginForm /> }
        </App>
    );
  }
}


OurApp.propTypes = {
  currentUser: React.PropTypes.object,
  allUsers: React.PropTypes.array
};


export default createContainer(() => {
  Meteor.subscribe('users');

  return {
    currentUser: Meteor.user(),
    allUsers: Meteor.user() ? Meteor.users.find({_id: {$ne: Meteor.user()._id}}).fetch() : null
  };
}, OurApp);
